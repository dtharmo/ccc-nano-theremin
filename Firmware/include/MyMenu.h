#ifndef MyMenu_H
#define MyMenu_H
#include <Arduino.h>

#define PLAY 0
#define TONE 1
#define SCALE 2
#define LENGTH 3

class MyMenu {

    private:
        int modes[4] = {PLAY, TONE, SCALE, LENGTH};
        int currentMode = 0;

    public:
        void next();
        void off();
        bool playing();
        int getMode();
};

void MyMenu::next() {
    currentMode ++;
    if (currentMode > 3) currentMode = 0;
}

void MyMenu::off() {
    currentMode = PLAY;
}

bool MyMenu::playing() {
    return (modes[currentMode] == PLAY) ? true : false;
}

int MyMenu::getMode() {
    return modes[currentMode];
}
#endif
