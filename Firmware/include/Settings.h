#ifndef Settings_H
#define Settings_H
#include <Arduino.h>
#include <ClickEncoder.h>
#include <TimerOne.h>
#include <MyMenu.h>

#define ENC_DT A0
#define ENC_CLK A1
#define ENC_SW A2
#define ENC_STEPS 4

ClickEncoder encoder(ENC_DT, ENC_CLK, ENC_SW, ENC_STEPS);

void timerIsr() {
    encoder.service();
}

class Settings {
    private:
        MyMenu _menu;
        int16_t oldEncPos, encPos;
        uint8_t buttonState;
        int _dir = 0;

    public:
        Settings(MyMenu);
        void begin();
        bool switched();
        bool changed = false;
        int direction();
        MyMenu menu();
};

Settings::Settings(MyMenu myMenu) {
    _menu = myMenu;
}

void Settings::begin() {
    Timer1.initialize(1000);
    Timer1.attachInterrupt(timerIsr);

    encoder.setAccelerationEnabled(true);
    oldEncPos = -1;
}

bool Settings::switched() {
    buttonState = encoder.getButton();
    encPos += encoder.getValue();

    if (encPos != oldEncPos) {
        if (encPos < oldEncPos) _dir = 1;
        else _dir = -1;
        oldEncPos = encPos;
        changed = true;
        return true;
    }
    else {
        changed = false;
    }

    if (buttonState == ClickEncoder::Clicked) {
        _menu.next();
        return true;
    }

    if (buttonState == ClickEncoder::DoubleClicked) {
        _menu.off();
        return true;
    }

    return false;
}

int Settings::direction() {
    return _dir;
}

MyMenu Settings::menu() {
    return _menu;
}
#endif
