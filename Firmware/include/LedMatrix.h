#ifndef LedMatrix_H
#define LedMatrix_H
#include <Arduino.h>

#define COL_1 6
#define COL_2 5
#define COL_3 4
#define COL_4 3
#define COL_5 2
#define ROW_1 10
#define ROW_2 9
#define ROW_3 8
#define ROW_4 7

int leds[][2] = {{COL_1, ROW_1}, {COL_2, ROW_1}, {COL_3, ROW_1}, {COL_4, ROW_1}, {COL_5, ROW_1},
                 {COL_1, ROW_2}, {COL_2, ROW_2}, {COL_3, ROW_2}, {COL_4, ROW_2}, {COL_5, ROW_2},
                 {COL_1, ROW_3}, {COL_2, ROW_3}, {COL_3, ROW_3}, {COL_4, ROW_3}, {COL_5, ROW_3},
                 {COL_1, ROW_4}, {COL_2, ROW_4}, {COL_3, ROW_4}, {COL_4, ROW_4}, {COL_5, ROW_4}};

class LedMatrix {
    private:
        void ledOn(int col, int row);
        void ledOff(int col, int row);
        int _previousLedOn[2] = {0, 0};

    public:
        void begin();
        void blink(int col, int row);
        void blinkForNote(int note);
        void select(int col, int row);
        void reset();
};

void LedMatrix::begin() {
    pinMode(COL_1, OUTPUT);
    pinMode(COL_2, OUTPUT);
    pinMode(COL_3, OUTPUT);
    pinMode(COL_4, OUTPUT);
    pinMode(COL_5, OUTPUT);
    pinMode(ROW_1, OUTPUT);
    pinMode(ROW_2, OUTPUT);
    pinMode(ROW_3, OUTPUT);
    pinMode(ROW_4, OUTPUT);

    digitalWrite(COL_1, HIGH);
    digitalWrite(COL_2, HIGH);
    digitalWrite(COL_3, HIGH);
    digitalWrite(COL_4, HIGH);
    digitalWrite(COL_5, HIGH);
    digitalWrite(ROW_1, LOW);
    digitalWrite(ROW_2, LOW);
    digitalWrite(ROW_3, LOW);
    digitalWrite(ROW_4, LOW);
}

void LedMatrix::blink(int col, int row) {
    ledOn(col, row);
    delay(50);
    ledOff(col, row);
}

void LedMatrix::blinkForNote(int note) {
    blink(leds[note][0], leds[note][1]);
}

void LedMatrix::ledOn(int col, int row) {
    digitalWrite(col, LOW);
    digitalWrite(row, HIGH);
}

void LedMatrix::ledOff(int col, int row) {
    digitalWrite(col, HIGH);
    digitalWrite(row, LOW);
}

void LedMatrix::select(int col, int row) {
    if (_previousLedOn[0] != 0) {
        ledOff(_previousLedOn[0], _previousLedOn[1]);
    }
    ledOn(col, row);
    _previousLedOn[0] = col;
    _previousLedOn[1] = row;
}

void LedMatrix::reset() {
    if (_previousLedOn[0] != 0) {
        ledOff(_previousLedOn[0], _previousLedOn[1]);
    }
    _previousLedOn[0] = 0;
    _previousLedOn[1] = 0;
}
#endif
