#define NOTE_C2  65.41
#define NOTE_CS2 69.30
#define NOTE_D2  73.42
#define NOTE_DS2 77.78
#define NOTE_E2  82.41
#define NOTE_F2  87.31
#define NOTE_FS2 92.50
#define NOTE_G2  98
#define NOTE_GS2 103.83
#define NOTE_A2  110
#define NOTE_AS2 116.54
#define NOTE_B2  123.47
#define NOTE_C3  130.81


double CHROMATIC[] = {NOTE_C2,  NOTE_C2,  NOTE_CS2, NOTE_D2,
                      NOTE_D2,  NOTE_DS2, NOTE_E2,  NOTE_E2,
                      NOTE_F2,  NOTE_F2,  NOTE_FS2, NOTE_G2,
                      NOTE_G2,  NOTE_GS2, NOTE_A2,  NOTE_A2,
                      NOTE_AS2, NOTE_B2,  NOTE_B2,  NOTE_C3};

double MINOR[] = {NOTE_C2,  NOTE_C2,  NOTE_D2,  NOTE_D2,
                  NOTE_DS2, NOTE_DS2, NOTE_DS2, NOTE_F2,
                  NOTE_F2,  NOTE_F2,  NOTE_G2,  NOTE_G2,
                  NOTE_GS2, NOTE_GS2, NOTE_GS2, NOTE_AS2,
                  NOTE_AS2, NOTE_AS2, NOTE_C3,  NOTE_C3};

double MAJOR[] = {NOTE_C2, NOTE_C2, NOTE_D2, NOTE_D2,
                  NOTE_E2, NOTE_E2, NOTE_E2, NOTE_F2,
                  NOTE_F2, NOTE_F2, NOTE_G2, NOTE_G2,
                  NOTE_A2, NOTE_A2, NOTE_A2, NOTE_B2,
                  NOTE_B2, NOTE_B2, NOTE_C3, NOTE_C3};

double DORIAN[] = {NOTE_C2,  NOTE_C2,  NOTE_D2,  NOTE_D2,
                   NOTE_D2,  NOTE_DS2, NOTE_DS2, NOTE_F2,
                   NOTE_F2,  NOTE_F2,  NOTE_G2,  NOTE_G2,
                   NOTE_G2,  NOTE_A2,  NOTE_A2,  NOTE_A2,
                   NOTE_AS2, NOTE_AS2, NOTE_C3,  NOTE_C3};

double AEOLIAN[] = {NOTE_C2,  NOTE_C2,  NOTE_D2,  NOTE_D2,
                    NOTE_D2,  NOTE_DS2, NOTE_DS2, NOTE_F2,
                    NOTE_F2,  NOTE_F2,  NOTE_G2,  NOTE_G2,
                    NOTE_G2,  NOTE_GS2, NOTE_GS2, NOTE_AS2,
                    NOTE_AS2, NOTE_AS2, NOTE_C3,  NOTE_C3};
