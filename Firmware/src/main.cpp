#include <Arduino.h>
#include <Ultrasonic.h>
#include "notes.h"
#include "LedMatrix.h"
#include "Settings.h"

#define TRIG 11
#define ECHO 12
#define MIN_DIST 3.0
#define MAX_DIST 30.0

#define OUT 13

#define TOTAL_NOTES 20
#define NOTE_DURATION 70

Ultrasonic ultrasonic(TRIG, ECHO);
int distance;

float stepNote = (MAX_DIST - MIN_DIST) / TOTAL_NOTES;
int currentNote;
double* mode[5] = {CHROMATIC, MINOR, MAJOR, DORIAN, AEOLIAN};
int currentMode = 0;
double currentToneOffset = 1.00;
int noteDuration[5] = {1, 70, 500, 1000, 0};
int currentDuration = 1;
int muted = 0;
int noDuration = 4;

LedMatrix ledMatrix;
int colArray[5] = {COL_1, COL_2, COL_3, COL_4, COL_5};
int rowArray[4] = {ROW_1, ROW_2, ROW_3, ROW_4};

MyMenu myMenu;
Settings settings(myMenu);

void setup() {
    Serial.begin(9600);

    pinMode(TRIG, OUTPUT);
    pinMode(ECHO, INPUT);

    ledMatrix.begin();

    settings.begin();
}

void playTone(){
    double freq = mode[currentMode][currentNote];
    for (int o = 2; o <= currentToneOffset; o++) {
      freq = freq * 2;
    }
    if (currentDuration == muted) noTone(OUT);
    else if (currentDuration == noDuration) tone(OUT, freq);
    else tone(OUT, freq, noteDuration[currentDuration]);

    ledMatrix.blinkForNote(currentNote);
}


void checkEncoder() {
  if (settings.switched()) {
    if (!settings.menu().playing()) {
      switch (settings.menu().getMode()) {
        case TONE:
          if (settings.changed) currentToneOffset += settings.direction();
          if (currentToneOffset > 4.00) currentToneOffset = 1.00;
          if (currentToneOffset < 1.00) currentToneOffset = 4.00;
          ledMatrix.select(COL_1, rowArray[(int)currentToneOffset - 1]);
          break;

        case SCALE:
          if (settings.changed) currentMode += settings.direction();
          if (currentMode > 4) currentMode = 4;
          if (currentMode < 0) currentMode = 0;
          ledMatrix.select(colArray[currentMode], ROW_1);
          break;

        case LENGTH:
          if (settings.changed) currentDuration += settings.direction();
          if (currentDuration > 4) currentDuration = 4;
          if (currentDuration < 0) currentDuration = 0;
          ledMatrix.select(colArray[currentDuration], ROW_4);
          break;

        default:
          break;
      }
    } else {
      ledMatrix.reset();
    }
  }
}


void loop() {
  checkEncoder();

  if (settings.menu().playing()) {
    distance = ultrasonic.read();
    if (distance >= MIN_DIST && distance <= MAX_DIST) {
      currentNote = distance / stepNote;
      if (currentNote > 19) currentNote = 19;
      if (currentNote < 0) currentNote = 0;

      playTone();
    }
  }
}
